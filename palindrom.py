"""

Задача на палиндром
https://www.codewars.com/kata/57a5015d72292ddeb8000b31

A palindrome is a word, phrase, number, or other sequence of characters which reads the
same backward or forward. This includes capital letters, punctuation, and word dividers.

Implement a function that checks if something is a palindrome. If the input is a number,
convert it to string first.

Examples(Input ==> Output)
"anna"   ==> true
"walter" ==> false
12321    ==> true
123456   ==> false

"""


def is_palindrome(string):
    pal = str(string)
    if pal[::] == pal[::-1]:
        return bool(1)
    else:
        return bool(0)


print(is_palindrome('anna'))
print(is_palindrome('ann'))
