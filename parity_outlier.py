"""

Find The Parity Outlier
https://www.codewars.com/kata/5526fc09a1bbd946250002dc

You are given an array (which will have a length of at least 3, but could be very large)
containing integers. The array is either entirely comprised of odd integers or entirely
comprised of even integers except for a single integer N. Write a method that takes the
array as an argument and returns this "outlier" N.

"""


def find_outlier(integers):

    flag_odd = 0
    flag_even = 0

    for i in range(len(integers)):
        if integers[i] % 2 == 0:
            flag_even += 1
        else:
            flag_odd += 1

    for i in range(len(integers)):
        if flag_even == (len(integers) - 1):
            if integers[i] % 2 != 0:
                return integers[i]
        else:
            if integers[i] % 2 == 0:
                return integers[i]


int_1 = [2, 4, 6, 8, 10, 3]
int_2 = [2, 4, 0, 100, 4, 11, 2602, 36]
int_3 = [160, 3, 1719, 19, 11, 13, -21]
print(f'"outlier" :{find_outlier(int_1)}')
print(f'"outlier" :{find_outlier(int_2)}')
print(f'"outlier" :{find_outlier(int_3)}')
